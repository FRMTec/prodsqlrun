/**
 * File Name: ExcelExport.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Export;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javafx.collections.ObservableList;
import prod.sql.run.Objects.ExcelExportInfo;
import prod.sql.run.Objects.Landi;
import prodsqlrun.Messages;

public class ExcelExport {

  private HSSFWorkbook workbook;
  private HSSFSheet singleSheet;
  private HSSFRow rowHead;
  private int headerColCount;
  private int colCount;

  public ExcelExport() {
    workbook = new HSSFWorkbook();
    headerColCount = 0;
    colCount = 0;
  }

  /**
   * Export all Data that are stored with addToExport()
   * @param filePath
   * @throws SQLException
   * @throws IOException
   */
  public void exportToExcel(String filePath) throws SQLException, IOException {
    FileOutputStream fos = new FileOutputStream(filePath);
    workbook.write(fos);
    fos.close();
    workbook.close();
  }

  public List<String> getColumnNames(ResultSet rs) throws SQLException {
    List<String> names = new ArrayList<String>();
    ResultSetMetaData metadata = rs.getMetaData();

    for (int i = 0; i < metadata.getColumnCount(); i++) {
      names.add(metadata.getColumnName(i + 1));
    }

    return names;
  }

  public void addToExport(ObservableList<String> columns, ObservableList<ObservableList<String>> rows, Landi landi, ExcelExportInfo info) {
    if (info.equals(ExcelExportInfo.singleTab)) {
      if (workbook.getNumberOfSheets() == 0) {
        singleSheet = workbook.createSheet(Messages.getString("result"));
      }
      rowHead = singleSheet.createRow(0);
      for (String header : columns) {
        rowHead.createCell(headerColCount).setCellValue(header);
        headerColCount++;
      }
      int rowCount = 1;
      for (ObservableList<String> obrow : rows) {
        HSSFRow row = singleSheet.createRow(rowCount);
        for (String string : obrow) {
          row.createCell(colCount).setCellValue(data);
          colCount++;
        }
        rowCount++;
      }
    }
    if (info.equals(ExcelExportInfo.multiTab)) {
      HSSFSheet sheet = workbook.createSheet(landi.getName().trim());
      HSSFRow rowHead = sheet.createRow(0);
      int headerColCount = 0;
      for (String header : columns) {
        rowHead.createCell(headerColCount).setCellValue(header);
        headerColCount++;
      }
      int rowCount = 1;
      for (ObservableList<String> obrow : rows) {
        HSSFRow row = sheet.createRow(rowCount);
        int colCount = 0;
        for (String data : obrow) {
          row.createCell(colCount).setCellValue(data);
          colCount++;
        }
        rowCount++;
      }
      formatExcel(landi.getName().trim());
    }
  }

  public void formatExcel(String sheetName) {
    int sheetIndex = workbook.getSheetIndex(sheetName);
    HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
    HSSFFont font = workbook.createFont();
    font.setBold(true);
    HSSFCellStyle style = workbook.createCellStyle();
    style.setFont(font);
    HSSFRow row = sheet.getRow(0);
    for (int counter = 0; counter < row.getPhysicalNumberOfCells(); counter++) {
      row.getCell(counter).setCellStyle(style);
    }
  }
}
