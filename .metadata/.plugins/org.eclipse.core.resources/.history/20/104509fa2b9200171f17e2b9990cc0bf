/**
 * File Name: ResultsGUI.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.GUI;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Duration;
import prod.sql.run.Export.ExcelExport;
import prod.sql.run.Objects.InfoType;
import prod.sql.run.Objects.Landi;
import prod.sql.run.Objects.ResultBorderPane;
import prod.sql.run.Objects.TableKeyEventHandler;
import prodsqlrun.Messages;

public class ResultsGUI extends Stage {

  private TableView<String> defaultTable;
  private ComboBox<Landi> comboBoxServers;

  private ArrayList<ResultBorderPane> panels;
  private BorderPane rootPane;
  private BorderPane panelWrapperTop;
  private static final String DEFAULT_SAVE_TEXT = "ProdSqlRunResults";
  private static final String EXPORT_SUCCESS = Messages.getString("exportSuccess");
  private static final String EXPORT_FAILED = Messages.getString("exportFailed");
  private Stage gui;
  private Label labelInfo;
  private boolean hasData;

  public ResultsGUI() {
    gui = this;
    panels = new ArrayList<>();
    rootPane = new BorderPane();
    panelWrapperTop = new BorderPane();

    initComboBoxServers();

    addMenu();

    panelWrapperTop.setLeft(comboBoxServers);

    labelInfo = new Label();
    panelWrapperTop.setCenter(labelInfo);

    rootPane.setTop(panelWrapperTop);

    defaultTable = new TableView<>();
    rootPane.setCenter(defaultTable);

    this.getIcons().add(new Image(this.getClass().getResourceAsStream("/gui/resources/icon.png")));

    Scene scene = new Scene(rootPane, 1000, 400);
    scene.getStylesheets().add(getClass().getResource("/gui/results/resources/RESULTS.css").toExternalForm());
    this.setScene(scene);

    this.addEventFilter(WindowEvent.WINDOW_SHOWING, new EventHandler<WindowEvent>() {

      @Override
      public void handle(WindowEvent event) {
        setFirst();
      }

    });
  }

  /**
   * Add Excel Export Menu
   */

  private void addMenu() {
    Label exportToExcel = new Label();
    exportToExcel.setOnMouseClicked(new EventHandler<MouseEvent>() {

      @Override
      public void handle(MouseEvent event) {
        showExportInformationDialog();
        excelExport();
      }

    });

    exportToExcel.setId("excelExport");
    panelWrapperTop.setRight(exportToExcel);

    RadioButton radioButtonSingleTag = new RadioButton(Messages.getString("singlet"));
    RadioButton radioButtonMultiTag = new RadioButton("Multi-Tab");

    HBox hBoxRadioButtons = new HBox();
    hBoxRadioButtons.getChildren().addAll(radioButtonSingleTag, radioButtonMultiTag);
    panelWrapperTop.setCenter(hBoxRadioButtons);
  }

  /**
   * Shows dialog for Excel export informaitons
   */
  private void showExportInformationDialog() {
    ArrayList<prod.sql.run.Objects.ExcelExport> exportDecisions = new ArrayList<>();
    exportDecisions.add(prod.sql.run.Objects.ExcelExport.SINGLE_TAG);
    exportDecisions.add(prod.sql.run.Objects.ExcelExport.MULTI_TAB);

    ChoiceDialog<prod.sql.run.Objects.ExcelExport> dialog = new ChoiceDialog<>(prod.sql.run.Objects.ExcelExport.MULTI_TAB, exportDecisions);
    dialog.setTitle(");

  }

  /**
   * FileChooser & Export to Excel
   */

  private void excelExport() {
    ExcelExport excelExport = new ExcelExport();
    for (ResultBorderPane pane : panels) {
      excelExport.addToExport(pane.getColumns(), pane.getRows(), pane.getLandi());
    }
    try {
      FileChooser fileChooser = new FileChooser();
      fileChooser.setTitle(Messages.getString("saveSelect"));
      fileChooser.setInitialFileName(DEFAULT_SAVE_TEXT);
      fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Excel", "*.xls"));
      File selectStatement_dir = fileChooser.showSaveDialog(gui);
      if (selectStatement_dir != null) {
        excelExport.exportToExcel(selectStatement_dir.getAbsolutePath());
        displayInfo(EXPORT_SUCCESS, InfoType.SUCCESS, 5);
      }
    } catch (SQLException | IOException e) {
      displayInfo(EXPORT_FAILED, InfoType.ERROR, 5);
      return;
    }
  }

  /**
   * @param exportSuccess
   * @param success
   * @param i
   */
  private void displayInfo(String info, InfoType infotype, int displayTime) {
    handleDisplay(info, infotype);
    PauseTransition showInfo = new PauseTransition(Duration.seconds(displayTime));
    showInfo.setOnFinished(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        panelWrapperTop.getStyleClass().clear();
        labelInfo.setText("");
      }
    });
    showInfo.play();
  }

  /**
   * @param info
   * @param infotype
   */
  private void handleDisplay(String info, InfoType infotype) {
    labelInfo.getStyleClass().clear();
    labelInfo.getStyleClass().add("labelInfo");
    if (infotype.equals(InfoType.ERROR)) {
      panelWrapperTop.getStyleClass().add("error");
    }
    if (infotype.equals(InfoType.WARNING)) {
      panelWrapperTop.getStyleClass().add("warning");
    }
    if (infotype.equals(InfoType.SUCCESS)) {
      panelWrapperTop.getStyleClass().add("success");
    }
    labelInfo.setText(info);
  }

  /**
   * Sets the first panel to the rootPane
   */
  public void setFirst() {
    if (panels.size() != 0) {
      rootPane.setCenter(panels.get(0));
      comboBoxServers.getSelectionModel().select(panels.get(0).getLandi());
    }
  }

  /**
   * 
   */
  private void initComboBoxServers() {
    comboBoxServers = new ComboBox<>();

    Callback cellFactory = new Callback<ListView<Landi>, ListCell<Landi>>() {

      @Override
      public ListCell<Landi> call(ListView<Landi> param) {
        return new ListCell<Landi>() {
          @Override
          protected void updateItem(Landi landi, boolean empty) {
            super.updateItem(landi, empty);
            if (landi == null || empty) {
              setGraphic(null);
            } else {
              setText(landi.getName().trim());
            }
          }
        };
      }

    };

    comboBoxServers.setButtonCell((ListCell<Landi>)cellFactory.call(null));
    comboBoxServers.setCellFactory(cellFactory);

    comboBoxServers.setOnAction(new EventHandler<ActionEvent>() {

      @Override
      public void handle(ActionEvent event) {
        for (ResultBorderPane resultBorderPane : panels) {
          if (resultBorderPane.getLandi().equals(comboBoxServers.getSelectionModel().getSelectedItem())) {
            rootPane.setCenter(resultBorderPane);
          }
        }
      }
    });
  }

  public void addResults(ResultSet rs, Landi landi) throws SQLException {
    ResultBorderPane resultBorderPane = new ResultBorderPane(landi, rs);
    TableView<ObservableList<String>> table = new TableView<>();
    table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

    int counter = 1;
    for (String column : resultBorderPane.getColumns()) {
      TableColumn<ObservableList<String>, String> col = new TableColumn<>(column);
      for (ObservableList<String> row : resultBorderPane.getRows()) {
        final int index = counter - 1;
        col.setCellValueFactory(

            cellData -> {

              ObservableList<String> rowData = cellData.getValue();
              if (index >= rowData.size()) {
                return new ReadOnlyStringWrapper("");
              } else {
                return new ReadOnlyStringWrapper(rowData.get(index));
              }
            });
      }
      table.getColumns().add(col);
      counter++;

    }

    table.setItems(resultBorderPane.getRows());
    table.getSelectionModel().setCellSelectionEnabled(true);
    table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    table.setOnKeyPressed(new TableKeyEventHandler());
    resultBorderPane.setCenter(table);

    // dont show results if there are no
    if (resultBorderPane.getRows().isEmpty()) {
      return;
    }

    comboBoxServers.getItems().add(landi);
    panels.add(resultBorderPane);
  }

  public void clear() {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        rootPane.setCenter(new TableView<String>());
        panels.clear();
        comboBoxServers.getItems().clear();
      }
    });
  }

  public boolean hasData() {
    hasData = false;
    for (ResultBorderPane resultBorderPane : panels) {
      if (!resultBorderPane.getRows().isEmpty() || !resultBorderPane.getColumns().isEmpty()) {
        hasData = true;
      }
    }
    return this.hasData;
  }

}