
package prod.sql.run.Log;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.junit.Test;

import prod.sql.run.Exception.ExecutionFailedException;
import prod.sql.run.Objects.Landi;

public class ProdSqlRunLoggerTest {

  @Test
  public void logger_SucceededLandi_stringContainsSucceed() {
    ProdSqlRunLogger logger = new ProdSqlRunLogger();
    ArrayList<Landi> executeLandi = new ArrayList<Landi>();
    executeLandi.add(new Landi("TestLandi", "", "", ""));
    ArrayList<ExecutionFailedException> errorList = new ArrayList<ExecutionFailedException>();
    logger.log(executeLandi, errorList, "TestStatement");
    assertTrue(logger.getLog().contains("SUCCESS"));
  }

  @Test
  public void logger_SucceededLandi_stringContainsLandiName() {
    ProdSqlRunLogger logger = new ProdSqlRunLogger();
    ArrayList<Landi> executeLandi = new ArrayList<Landi>();
    executeLandi.add(new Landi("TestLandi", "", "", ""));
    ArrayList<ExecutionFailedException> errorList = new ArrayList<ExecutionFailedException>();
    logger.log(executeLandi, errorList, "TestStatement");
    assertTrue(logger.getLog().contains("TestLandi"));
  }

  @Test
  public void logger_SucceededLandi_stringContainsStatement() {
    ProdSqlRunLogger logger = new ProdSqlRunLogger();
    ArrayList<Landi> executeLandi = new ArrayList<Landi>();
    executeLandi.add(new Landi("TestLandi", "", "", ""));
    ArrayList<ExecutionFailedException> errorList = new ArrayList<ExecutionFailedException>();
    logger.log(executeLandi, errorList, "TestStatement");
    assertTrue(logger.getLog().contains("TestStatement"));
  }

  @Test
  public void logger_FailedLandi_stringContainsFailed() {
    ProdSqlRunLogger logger = new ProdSqlRunLogger();
    ArrayList<Landi> executeLandi = new ArrayList<Landi>();
    executeLandi.add(new Landi("TestLandi", "", "", ""));
    ArrayList<ExecutionFailedException> errorList = new ArrayList<ExecutionFailedException>();
    ExecutionFailedException executionFailedException = new ExecutionFailedException("TestMessage");
    executionFailedException.setLandi(new Landi("TestLandi", "", "", ""));
    errorList.add(executionFailedException);
    logger.log(executeLandi, errorList, "TestStatement");
    assertTrue(logger.getLog().contains("FAILED"));
  }

  @Test
  public void logger_storeLog_ExistingFile() throws IOException {
    ProdSqlRunLogger logger = new ProdSqlRunLogger();
    ArrayList<Landi> executeLandi = new ArrayList<Landi>();
    executeLandi.add(new Landi("TestLandi", "", "", ""));
    ArrayList<ExecutionFailedException> errorList = new ArrayList<ExecutionFailedException>();
    logger.log(executeLandi, errorList, "TestStatement");
    new File(System.getProperty("java.io.tmpdir") + "\\prodSQLRunTst").mkdir();
    logger.storeLog(System.getProperty("java.io.tmpdir") + "\\prodSQLRunTst");
    assertThat(new File(System.getProperty("java.io.tmpdir") + "\\prodSQLRunTst").list().length, is(not(0)));
    cleanup();
  }

  private void cleanup() {
    File dir = new File(System.getProperty("java.io.tmpdir") + "\\prodSQLRunTst");
    File[] files = dir.listFiles();
    for (File file : files) {
      file.delete();
    }
    dir.delete();
  }
}
