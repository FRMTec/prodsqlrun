/**
 * File Name: DataSource.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.DataSource;

import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import prod.sql.run.Manager.ProdSqlRunManager;
import prod.sql.run.Objects.Landi;

/**
 * @author elias.broger
 */
public class DataSource {

  private String lastloadDataPath;
  private ProdSqlRunManager prodSqlRunManager;

  public DataSource() {
    prodSqlRunManager = new ProdSqlRunManager();
    lastloadDataPath = System.getProperty("user.home");
  }

  public ArrayList<Landi> getLandis(String path, String fileName) throws ParserConfigurationException, SAXException, IOException {
    Document doc = loadFile(path + "/" + fileName);
    lastloadDataPath = path;
    ArrayList<Landi> loadedLandis = new ArrayList<>();
    loadedLandis = loadToArray(doc);
    for (Landi landi : loadedLandis) {
      if (landi.getConnectionString().contains("bisdevlandi.bisdevdom.ch")) {
        landi.setPassword("chlandidml");
      } else {
        landi.setPassword(landi.getUsername());
      }
      if (landi.getName().contains("EDUC")) {
        landi.setDataFrom(prodSqlRunManager.getEducData(landi));
      }
    }
    return loadedLandis;
  }

  public String getLastLoadDataPath() {
    return this.lastloadDataPath;
  }

  private ArrayList<Landi> loadToArray(Document doc) {
    String name = "";
    String connection = "";
    String username = "";
    String decriptedPassword = "";
    NodeList nodeList = doc.getElementsByTagName("Reference");
    ArrayList<Landi> landiList = new ArrayList<Landi>();
    double nodeListLength = nodeList.getLength();
    for (int counter = 0; counter < nodeListLength; counter++) {
      Node node = nodeList.item(counter);

      if (node.getNodeType() == Node.ELEMENT_NODE) {

        Element element = (Element)node;

        NodeList fieldNodes = element.getElementsByTagName("StringRefAddr");
        double fieldNodesLength = fieldNodes.getLength();
        for (int i = 0; i < fieldNodesLength; i++) {
          Node fieldNode = fieldNodes.item(i);
          NamedNodeMap attributes = fieldNode.getAttributes();
          Node attr = attributes.getNamedItem("addrType");
          if (attr != null) {
            if (attr.getTextContent().equals("ConnName")) {
              name = fieldNode.getTextContent();
            }
            if (attr.getTextContent().equals("customUrl")) {
              connection = fieldNode.getTextContent();
            }
            if (attr.getTextContent().equals("user")) {
              username = fieldNode.getTextContent();
            }
            if (attr.getTextContent().equals("password")) {
              decriptedPassword = fieldNode.getTextContent();
            }
          }
        }
        landiList.add(new Landi(name, connection, username, decriptedPassword));
      }
    }
    return landiList;
  }

  /**
   * load xml path from custom path
   * 
   * @param path to xml file
   * @return document
   */
  public Document loadFile(String path) throws ParserConfigurationException, SAXException, IOException {

    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    Document document = dBuilder.parse(path);

    return document;
  }
}
