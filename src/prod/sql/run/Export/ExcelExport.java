/**
 * File Name: ExcelExport.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Export;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javafx.collections.ObservableList;
import prod.sql.run.Objects.ExcelExportInfo;
import prod.sql.run.Objects.Landi;
import prodsqlrun.Messages;

public class ExcelExport {

  private HSSFWorkbook workbook;
  private HSSFSheet singleSheet;
  private int headerColCount;
  private int colSingleTabCount;
  private int rowSingleTabCount;
  private String statement;

  public ExcelExport(String statement) {
    setStatement(statement);
    workbook = new HSSFWorkbook();
    this.headerColCount = 0;
    this.colSingleTabCount = 0;
  }

  public void exportToExcel(String filePath) throws IOException {
    addStatementSheet();
    FileOutputStream fos = new FileOutputStream(filePath);
    workbook.write(fos);
    fos.close();
    workbook.close();
  }

  public List<String> getColumnNames(ResultSet rs) throws SQLException {
    List<String> names = new ArrayList<String>();
    ResultSetMetaData metadata = rs.getMetaData();

    for (int i = 0; i < metadata.getColumnCount(); i++) {
      names.add(metadata.getColumnName(i + 1));
    }

    return names;
  }

  public void addToExport(ObservableList<String> columns, ObservableList<ObservableList<String>> rows, Landi landi, ExcelExportInfo info) {
    if (info.equals(ExcelExportInfo.singleTab)) {
      if (this.workbook.getNumberOfSheets() == 0) {
        createHeadRowSingleTab(columns);
      }
      insertDataSingleTab(rows, landi);
      formatExcel(Messages.getString("result"));
    }
    if (info.equals(ExcelExportInfo.multiTab)) {
      HSSFSheet sheet = this.workbook.createSheet(landi.getName().trim());
      createHeadRowMultiTab(columns, sheet);
      insertDataMultiTab(rows, sheet);
      formatExcel(landi.getName().trim());
    }
  }

  private void insertDataMultiTab(ObservableList<ObservableList<String>> rows, HSSFSheet sheet) {
    int rowCount = 1;
    for (ObservableList<String> obrow : rows) {
      HSSFRow row = sheet.createRow(rowCount);
      int colCount = 0;
      for (String data : obrow) {
        row.createCell(colCount).setCellValue(data);
        colCount++;
      }
      rowCount++;
    }
  }

  private void createHeadRowMultiTab(ObservableList<String> columns, HSSFSheet sheet) {
    HSSFRow rowHead = sheet.createRow(0);
    int headerColCount = 0;
    for (String header : columns) {
      rowHead.createCell(headerColCount).setCellValue(header);
      headerColCount++;
    }
  }

  private void insertDataSingleTab(ObservableList<ObservableList<String>> rows, Landi landi) {
    this.rowSingleTabCount++;
    this.singleSheet.createRow(this.rowSingleTabCount);
    for (ObservableList<String> obrow : rows) {
      this.colSingleTabCount = 0;
      HSSFRow row = this.singleSheet.createRow(this.rowSingleTabCount);
      for (String data : obrow) {
        row.createCell(this.colSingleTabCount).setCellValue(data);
        this.colSingleTabCount++;
      }
      row.createCell(this.colSingleTabCount + 2).setCellValue(landi.getName().trim());
      this.rowSingleTabCount++;
    }
  }

  private void createHeadRowSingleTab(ObservableList<String> columns) {
    this.singleSheet = this.workbook.createSheet(Messages.getString("result"));
    this.rowSingleTabCount = 0;
    HSSFRow rowHead = this.singleSheet.createRow(0);
    for (String header : columns) {
      rowHead.createCell(this.headerColCount).setCellValue(header);
      this.headerColCount++;
    }
  }

  private void addStatementSheet() {
    HSSFSheet statementSheet = this.workbook.createSheet(Messages.getString("statement"));
    HSSFRow row = statementSheet.createRow(0);
    row.createCell(0).setCellValue(this.statement);
  }

  public void formatExcel(String sheetName) {
    int sheetIndex = workbook.getSheetIndex(sheetName);
    HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
    HSSFFont font = workbook.createFont();
    font.setBold(true);
    HSSFCellStyle style = workbook.createCellStyle();
    style.setFont(font);
    HSSFRow row = sheet.getRow(0);
    for (int counter = 0; counter < row.getPhysicalNumberOfCells(); counter++) {
      row.getCell(counter).setCellStyle(style);
    }
  }

  public String getStatement() {
    return statement;
  }

  public void setStatement(String statement) {
    this.statement = statement;
  }
}
