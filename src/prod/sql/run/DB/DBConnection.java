/**
 * File Name: DBManager.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.DB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import prod.sql.run.Exception.ExecutionFailedException;
import prod.sql.run.Objects.Landi;

/**
 * @author elias.broger
 */
public class DBConnection {

  private static final String DATETIME_FORMAT = "alter session set nls_date_format = 'YYYY-MM-DD HH24:MI:SS'";
  private Connection conn;
  private Landi connLandi;

  public DBConnection() {
    try {
      Class.forName("oracle.jdbc.driver.OracleDriver").newInstance();
      connLandi = new Landi("", "", "", "");
    } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  public Connection getConnection(Landi landi) throws ExecutionFailedException {
    try {
      if (!this.connLandi.equals(landi)) {
        this.conn = DriverManager.getConnection(landi.getConnectionString().trim(), landi.getUsername().trim(), landi.getPassword().trim());
        this.connLandi = landi;
      }
      return this.conn;
    } catch (Exception e) {
      ExecutionFailedException ce = new ExecutionFailedException(e.getMessage());
      ce.setLandi(landi);
      throw ce;
    }
  }

  public ResultSet execute(String sqlStatement, Landi landi, boolean autocommit) throws ExecutionFailedException {
    ResultSet rs = null;
    Statement stmt = null;
    Connection executeConn = null;
    try {
      executeConn = getConnection(landi);
      executeConn.setAutoCommit(autocommit);
      stmt = executeConn.createStatement();
      stmt.execute(DATETIME_FORMAT);
      if (stmt.execute(sqlStatement)) {
        rs = stmt.getResultSet();
        return rs;
      }
    } catch (Exception e) {
      throw new ExecutionFailedException(e.getMessage());
    }
    return null;
  }
}