/**
 * File Name: ProdSqlRunLogger.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import prod.sql.run.Exception.ExecutionFailedException;
import prod.sql.run.Objects.Landi;
import prod.sql.run.Objects.LogLevel;

public class ProdSqlRunLogger {

  private StringBuilder log;
  private StringBuilder header;
  private StringBuilder logData;
  private String name;

  public ProdSqlRunLogger() {
    logData = new StringBuilder();
    header = new StringBuilder();
    header.append(
        "<style>table, th, td { border: 1px solid black; border-collapse: collapse; }.error{ background-color: rgba(255,0,0,0.5); } .success{ background-color: rgba(102, 255, 51,0.5);}</style>");
    DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Calendar cal = Calendar.getInstance();
    String currentDateTime = df.format(cal.getTime());
    header.append("<h1>Log created: ");
    header.append(currentDateTime);
    header.append("</h1>");
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd-HHmm");
    this.name = "prodSqlRunlog_" + simpleDateFormat.format(Calendar.getInstance().getTime());
  }

  /**
   * persist log to custom path
   * 
   * @param dir directory
   * @throws IOException
   */
  public void storeLog(String dir) throws IOException {
    File file = new File(dir + "\\" + this.name + ".html");
    file.createNewFile();
    FileWriter writer = new FileWriter(file.getPath(), true);
    writer.write(getLog());
    writer.close();
  }

  /**
   * @param executeLandi
   * @param errorList
   */
  public void log(ArrayList<Landi> executeLandi, ArrayList<ExecutionFailedException> errorList, String statement) {
    StringBuilder newLog = new StringBuilder();

    newLog.append("<table style=\"width:100%\">");
    newLog.append("<tr><td>Statement</td>");
    newLog.append("<td>");
    if (statement.length() >= 40) {
      newLog.append(statement.substring(0, 41));
    } else {
      newLog.append(statement);
    }
    newLog.append("</td></tr>");
    for (Landi landi : executeLandi) {
      boolean hasExcetption = false;
      for (ExecutionFailedException exception : errorList) {
        if (landi.getName().equals(exception.getLandi().getName())) {
          hasExcetption = true;
          newLog.append("<tr class=\"error\"><td>");
          newLog.append(landi.getName());
          newLog.append("</td><td>");
          newLog.append("FAILED");
          newLog.append("</td></tr><tr class=\"error\"><td colspan=\"2\">");
          newLog.append(exception.getMessage());
          newLog.append("</td></tr>");
        }
      }
      if (!hasExcetption) {
        newLog.append("<tr class=\"success\"><td>");
        newLog.append(landi.getName());
        newLog.append("</td><td>");
        newLog.append("SUCCESS");
        newLog.append("</td></tr>");
      }
    }
    newLog.append("</table>");
    newLog.append("<br><br>");
    logData.insert(0, newLog);
  }

  public String getLog() {
    log = new StringBuilder();
    log.append(header);
    log.append(logData);
    return log.toString();
  }

  public void log(String logText, LogLevel logLevel) {
    StringBuilder builder = new StringBuilder();
    builder.append("<table style=\"width:100%\">");
    switch (logLevel) {
      case ERROR:
        builder.append("<tr class=\"error\"><td>");
        break;
      case WARNING:
        builder.append("<tr class=\"warning\"><td>");
        break;
      case INFO:
        builder.append("<tr class=\"info\"><td>");
        break;
      default:
        break;
    }
    builder.append(logText);
    builder.append("</td></tr>");
    builder.append("</table>");
    builder.append("<br><br>");
    logData.insert(0, builder);
  }

}
