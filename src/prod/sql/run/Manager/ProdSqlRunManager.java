/**
 * File Name: ProdSqlRunManager.java
 * 
 * Copyright (c) 2016 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Manager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import com.sun.media.jfxmedia.logging.Logger;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import prod.sql.run.DB.DBConnection;
import prod.sql.run.Exception.ExecutionFailedException;
import prod.sql.run.GUI.PRODSQLRUN;
import prod.sql.run.GUI.ResultsGUI;
import prod.sql.run.Objects.Landi;
import prod.sql.run.Objects.LandiComperator;

public class ProdSqlRunManager {

  private DBConnection dbConnection;
  private static final String EDUC_DATA = "Select MBANAME from bas$mba where MBANAME like '%LANDI%'";
  private PRODSQLRUN gui;
  private ResultsGUI rsGUI;
  private ArrayList<String> statements;

  public ProdSqlRunManager() {
    dbConnection = new DBConnection();
  }

  public ProdSqlRunManager(PRODSQLRUN gui, ResultsGUI rsGUI) {
    dbConnection = new DBConnection();
    this.gui = gui;
    this.rsGUI = rsGUI;
  }

  public ObservableList<Landi> sortLandis(ObservableList<Landi> listToSort) {
    FXCollections.sort(listToSort, new LandiComperator());
    return listToSort;
  }

  /**
   * execute custom sql statments on given amout of servers
   * 
   * @param sqlStatement statement to execute
   * @param servers server where statement should be executed
   * @param autocommit
   * @return List of errors that appear while executing the statement
   */
  public ArrayList<ExecutionFailedException> execute(String sqlStatement, ArrayList<Landi> servers, boolean autocommit) {
    formatStatement(sqlStatement);
    clearDisplay();
    ArrayList<ExecutionFailedException> errorList = new ArrayList<ExecutionFailedException>();
    ResultSet rs = null;
    for (Landi landi : servers) {
      for (String statement : statements) {
        updateGui(landi);
        try {
          rs = dbConnection.execute(statement, landi, autocommit);
          if (rs != null) {
            addResults(rs, landi, statement);
          }
        } catch (ExecutionFailedException e) {
          e.setLandi(landi);
          errorList.add(e);
          continue;
        }
      }
    }
    if (rsGUI.hasData()) {
      showResults();
    }
    return errorList;
  }

  /**
   * format statement to multiple substatements splitet by a semicolon
   * 
   * @param sqlStatement
   */
  public void formatStatement(String sqlStatement) {
    StringBuilder statement = new StringBuilder(sqlStatement);
    statements = new ArrayList<>();
    if (!sqlStatement.contains(";")) {
      this.statements.add(sqlStatement);
      return;
    }

    while (statement.toString().contains(";")) {
      int indexOfSeparator = statement.indexOf(";");
      if (!separatorIsInComment(statement.substring(0, indexOfSeparator + 1))) {
        this.statements.add(statement.substring(0, indexOfSeparator));
        statement.delete(0, indexOfSeparator + 1);
      } else {
        int lastSeparator = statement.indexOf(";");
        while (separatorIsInComment(statement.substring(0, lastSeparator))) {
          lastSeparator = statement.indexOf(";", lastSeparator + 1);
        }
        this.statements.add(statement.substring(0, lastSeparator));
        statement.delete(0, lastSeparator + 1);
      }
    }
  }

  private boolean separatorIsInComment(final String statement) {
    int open = StringUtils.countMatches(statement, "/*");
    int closed = StringUtils.countMatches(statement, "*/");
    int stringChars = StringUtils.countMatches(statement, "'");
    if (statement.contains("--")) {
      int index = statement.lastIndexOf("--");
      if (!statement.substring(index, statement.length()).contains("\n")) {
        return true;
      }
    }
    return open != closed || stringChars % 2 != 0;
  }

  public ArrayList<String> getStatements() {
    return this.statements;
  }

  /**
   * checks for comments in the statement
   * 
   * @param statement which should be checked
   * @return boolean if comment is fount or not
   */
  private boolean hasCommands(String statement) {
    int open = StringUtils.countMatches(statement, "/*");
    int closed = StringUtils.countMatches(statement, "*/");
    int stringChars = StringUtils.countMatches(statement, "'");
    if (open == closed && stringChars % 2 == 0) {
      statements.add(statement);
      return true;
    } else {
      return false;
    }
  }

  private void clearDisplay() {
    rsGUI.clear();
  }

  /**
   * show stage
   */
  private void showResults() {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        rsGUI.setFirst();
        rsGUI.show();
      }
    });
  }

  private void addResults(ResultSet rs, Landi landi, String statement) {
    rsGUI.addResults(rs, landi, statement);
  }

  /**
   * set name of landi to popup
   * 
   * @param landi
   */
  private void updateGui(Landi landi) {
    Platform.runLater(new Runnable() {

      @Override
      public void run() {
        if (gui != null) {
          gui.updatePopup(landi.getName().trim());
        }
      }
    });
  }

  /**
   * @param landi
   * @return The MBAName of the Landi.
   */
  public String getEducData(Landi landi) {
    ResultSet rs = null;
    try {
      rs = dbConnection.execute(EDUC_DATA, landi, true);
      if (rs == null) {
        return null;
      }
      while (rs.next()) {
        return rs.getString("MBANAME");
      }
    } catch (ExecutionFailedException | SQLException e) {
      Logger.logMsg(0, e.getMessage());
      return null;
    }
    return null;
  }

  public void saveSQLFile(String statementToSave, File saveFile) throws IOException {
    if (!saveFile.exists()) {
      saveFile.createNewFile();
    }
    Files.write(Paths.get(saveFile.getAbsolutePath()), statementToSave.getBytes());
  }

}
