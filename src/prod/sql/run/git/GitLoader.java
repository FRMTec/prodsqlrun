/**
 * File Name: GitLoader.java
 * 
 * Copyright (c) 2018 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.git;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

public class GitLoader {

  private File gitdirectory;
  private String username;
  private String password;

  public GitLoader(String username, String password) {
    this.username = username;
    this.password = password;
    gitdirectory = new File(System.getProperty("user.home") + "\\.prodsqlrun");
  }

  private Git cloneRepository(File dir) throws InvalidRemoteException, TransportException, GitAPIException {
    return Git.cloneRepository().setURI(String.format("https://%s@bisdevgit.bisdevdom.ch:8443/r/BisonProcess/LANDI/LandiSQL.git", username))
        .setDirectory(dir).call();
  }

  public File getFileFromGit() throws InvalidRemoteException, TransportException, GitAPIException, IOException {
    Git git;
    if (!gitdirectory.exists()) {
      gitdirectory.mkdir();
      git = cloneRepository(gitdirectory);
    } else {
      Repository localRepo = new FileRepository(gitdirectory.getAbsolutePath() + "/.git");
      git = new Git(localRepo);
      PullCommand pullCmd = git.pull();
      pullCmd.call();
    }

    return gitdirectory;
  }

  public Iterable<PushResult> commitFile(File saveFile, String message)
      throws IOException, InvalidRemoteException, TransportException, GitAPIException {
    Git git = Git.open(new File(gitdirectory.getAbsolutePath()));
    git.add().addFilepattern(".").call();
    git.commit().setCommitter(this.username, String.format("%s@bison-group.com", this.username)).setMessage(message).call();

    Git gitPush = Git.open(new File(gitdirectory.getAbsolutePath()));
    PushCommand pushCmd = gitPush.push().setCredentialsProvider(new UsernamePasswordCredentialsProvider(this.username, this.password));
    Iterable<PushResult> result = pushCmd.call();
    return result;
  }

}
