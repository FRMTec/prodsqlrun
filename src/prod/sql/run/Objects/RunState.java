/**
 * File Name: RunState.java
 * 
 * Copyright (c) 2018 BISON Schweiz AG, All Rights Reserved.
 */

package prod.sql.run.Objects;

public enum RunState {
  ERROR, NEUTRAL, SUCCESS;
}
